<?php get_header(); the_post(); 
$content_blocks = get_field('content_blocks');
$intro_quote = get_field('intro_quote');


$recent_posts = get_posts(array(
	'post_type' => 'post',
	'numberposts' => 4,
	'post_status' => 'publish',
	'order' => 'DESC',
	'orderby' => 'post_date'
));

?>
	
    <div id="main" class="clearfix">
        

        <?php if($content_blocks): ?>
        
             
			<?php foreach($content_blocks as $content_block): 
			?>
              
            <div class="home_block <?= $content_block['block_slug'] ?>" 
            <?php if($content_block['block_attachment']) : ?> style="background-image: url(<?= $content_block['block_attachment'] ?>)" <?php endif; ?>> 
            
            	<div class="section_block_content"> 
                 
                <?php if($content_block['block_title']): ?>
                    <div class="home_title"><h2><?=$content_block['block_title'] ?></h2></div>
                <?php endif; ?>
                
                <?php if($content_block['block_content']): ?>
                    <?= $content_block['block_content'] ?>
                <?php endif; ?>
                
				<?php if($content_block['block_link']): ?>
                    <div class="learn_btn"><a href="<?= $content_block['block_link'] ?>"><div class="learn_icon"></div>learn more</a></div>
                <?php endif; ?>
                
                
                </div><!--.section_block_content-->
                
            </div><!--.home_block-->    
                
                
                
            <?php endforeach; ?>
            
        
        <?php endif;?>
    	    
    	
 
      
        
            
            <?php // news section
			if($recent_posts): ?>
            
            	 <div class="section_title second"><h3>From the Blog</h3></div>
                
                 <div class="section_main_content">
                    
                      <div class="recent_posts">
                        <?php foreach($recent_posts as $post): setup_postdata($post); ?>
                            <div class="two_column">
                            	<div class="news_category"><?php the_category(); ?></div>
                                <div class="news_content">
                                    <span class="news_date"><?php the_time('F jS, Y'); ?></span>
                                    <a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>"><h4><?php the_title(); ?></h4></a>
                                    <span class="news_excerpt"><?php the_excerpt(); ?></span>
                               </div>
                            </div>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                      </div><!-- .recent_posts -->
                   </div><!--.section_main_content-->
            <?php endif; ?>

       
        
        
    </div><!-- #main -->
   


<?php get_footer(); ?>
	
   
	
