<?php
/**
 *
 * Default Page Header
 *
 * @package WP-Bootstrap
 * @subpackage Default_Theme
 * @since WP-Bootstrap 0.1
 */ ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
   <title><?php
  /*
   * Print the <title> tag based on what is being viewed.
   */
  global $page, $paged;

  wp_title( '|', true, 'right' );

  // Add the blog name.
  bloginfo( 'name' );

  // Add the blog description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) )
    echo " | $site_description";

  // Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 )
    echo ' | ' . sprintf( __( 'Page %s', 'bootstrapwp' ), max( $paged, $page ) );

  ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
   
    <!--<meta name="viewport" content="width=980" /> -->
   

    <link rel="profile" href="http://gmpg.org/xfn/11" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,800italic,400,800,600,700' rel='stylesheet' type='text/css'>
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

  <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="<?php bloginfo( 'template_url' );?>/favicon.ico" />
    <link rel="apple-touch-icon" href="<?php bloginfo( 'template_url' );?>/images/ctt_icon_57x57.png" />

  <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
  
  
  <?php wp_head(); ?>
  
  <script src="<?php bloginfo('template_url'); ?>/js/modernizr-1.7.min.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/jquery.scrollTo.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/jquery.flexslider.js"></script>
    
  
  </head>
  <body <?php body_class(); ?>>
  
  <div id="wrapper_all">
  <div id="wrapper">
  
  <div id="header_wrap">
  	  
      <header id="branding" role="banner">
        
            <hgroup id="headergroup">
                <h1 id="site_title">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="home_link">
                        <img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="" />
                        <span><?php bloginfo( 'name' ); ?></span>
                    </a>
                </h1>
                <h4 class="subheader"><?php bloginfo('description'); ?></h4>
            </hgroup> 
            
            <nav id="access" role="navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
            </nav><!-- #access -->
   	   
       
    </header><!-- #branding -->		
   	  
  </div><!-- #header_wrap -->
   
   
   