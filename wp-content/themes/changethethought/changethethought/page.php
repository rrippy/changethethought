<?php
get_header(); the_post(); 

$letter_code = get_field('letter_code');
$page_headline = get_field('page_headline');
$page_images = get_field('page_images');
$page_sections = get_field('page_sections');
$address = get_field('address');
$other_contact = get_field('other_contact');

?>
	
    <div id="main" class="clearfix">
    
    	<header class="main_title">
        	
            <?php if ($letter_code) { ?>
                <div class="letter_code"><?= $letter_code ?></div>
            <?php } ?>
            
            <?php if ($page_headline) { ?>
                <h1><?= $page_headline ?></h1>
            <?php } ?>
        
        </header>
        
        
        
         <?php if($page_images): ?>
        
                
                <div class="flexslider">
                  <ul class="slides">
                  	<?php foreach($page_images as $page_image): ?>
                        <li>
                          <img src="<?= $page_image['page_image'] ?>" />
                          <?php if($page_image['image_title']): ?>
                            	<h3 class="flex-caption"><?=$page_image['image_title'] ?></h3>
                          <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                  </ul><!--.slides--> 
                </div><!--.flexslider--> 
 
            
 
        <?php endif;?>
        
        
        

        <?php if($page_sections): ?>
        
        	<div class="section_block_content"> 
          
			<?php foreach($page_sections as $page_section): ?>
              
              <div class="section_block">
                 
                <?php if($page_section['section_title']): ?>
                    <h2><?= $page_section['section_title'] ?></h2>
                <?php endif; ?>
                
                <?php if($page_section['section_text']): ?>
                    <div class="section_text"><?= $page_section['section_text'] ?></div>
                <?php endif; ?>
              
              </div><!--.section_block-->  
                    
            <?php endforeach; ?>
            
            </div><!--.section_block_content--> 

        <?php endif;?>
    	   
		
        
        
        <?php if ($address) { ?>
                <div class="contact_block">
                	<div class="contact_title">Inquire:</div><?= $address ?>
                	<div class="google_link"><a href="https://maps.google.com/maps?q=8100+West+10th+Avenue,+Lakewood,+Colorado+80214,+USA&hl=en&sll=37.0625,-95.677068&sspn=56.200193,135.263672&oq=8100+W+10th+Avenue+Lakewood,+Colorado+80214+USA&t=m&hnear=8100+W+10th+Ave,+Lakewood,+Jefferson,+Colorado+80214&z=17" target="_blank">Google Maps Quicklink.</a></div>
                </div>
        <?php } ?>
        
        <?php if ($other_contact) { ?>
                <div class="contact_block"><div class="contact_title">Hire:</div><?= $other_contact ?></div>
        <?php } ?>
            
            
       
        <div class="blog_line">Inspiration is sacred. And so is our blog. That’s why it’s <a href="http://www.changethethought.com/">separate</a>.</div>
        
    </div><!-- #main -->
   


<?php get_footer(); ?>
	
   
	
