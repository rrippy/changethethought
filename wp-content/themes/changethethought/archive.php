<?php


get_header(); ?>

    <div id="main" class="clearfix">
    
    
    
    <div class="section_main_content clearfix">
    
        
        <div id="primary">	
		<?php while ( have_posts() ) : the_post(); ?>
  		
        
        	 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
             <a href="<?php the_permalink(); ?>" class="work_thumb">
             
             	<?php the_post_thumbnail(); ?>
                
                <div class="work_info">
                
					<?php $currentID = get_the_ID(); ?>
                    <?php $currentNumber = Get_Post_Number($currentID); ?>
                    <div class="post_number"><?php echo $currentNumber; ?></div>
                    
                    <?php $letter_code = get_field('letter_code'); ?>
                    <div class="letter_code"><?= $letter_code ?></div>
                
                 </div><!--.work_info-->
                
             
                <div class="work_hover_content">
                
                	<header>
                        <h2><?php the_title();?></h2>
                        <div class="work_tagged"><strong>Tags: </strong> 
							<?php $category = get_the_category(); 
								echo $category[0]->cat_name; ?>
                        </div>
                        <div class="view_project">View Project</div>
                    </header>
                  
                </div><!--.work_hover_content-->

                
           </a> <!--.work_thumb-->  
           </article>
           	
        
        <?php endwhile; // End the loop ?>
        
        
        <div class="top_button"><a href="#">Top</a></div>

        
        </div><!-- #primary -->
        
        
        
        
        </div><!-- .section_main_content -->
        
        
    </div><!-- #main -->    



<?php get_footer(); ?>