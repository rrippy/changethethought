$(document).ready(function(){
	
	// arrow button fades
  	$('.next_arrow a, .previous_arrow a').hover(function() {
		$(this).animate({opacity: '0.5'});
	}, function () {
		$(this).animate({opacity: '0.3'});
	});
	
	// piece button fades
  	$('body.home article a, body.archive article a').hover(function() {
		$(this).children('.work_hover_content').css('visibility','visible').hide().fadeIn(350);
		$(this).children('.work_info').fadeOut(250);
	}, function () {
		$(this).children('.work_hover_content').fadeOut(250);
		$(this).children('.work_info').fadeIn(350);
	});
	
	// scroll to top
	$('.top_button a').click(function(e) {
		e.preventDefault();
		// scroll document
		$.scrollTo(0,'slow');
   });
   // remove button if less than 6 items
   var numArticles = $('#primary').children('article').length;
   if (numArticles < 6) {
	   $('body.home .top_button').hide();
	   $('body.archive .top_button').hide();
   }
   
  
  
   
   
   // load more pieces
   var moreBtn = 0;
	$('.more_button').click(function(e){
		e.preventDefault();
		moreBtn = (moreBtn + 1);
		
		if (moreBtn == 1) {
			$('#more_posts_wrapper').load('more-posts/', function() {
				$('body.home article a, body.archive article a').hover(function() {
					$(this).children('.work_hover_content').css('visibility','visible').hide().fadeIn(350);
					$(this).children('.work_info').fadeOut(250);
				}, function () {
					$(this).children('.work_hover_content').fadeOut(250);
					$(this).children('.work_info').fadeIn(350);
				});
			});
				
			return false;
		}
		else if (moreBtn == 2) {
			$('#more_posts_wrapper-2').load('more-posts-2/', function() {
				$('body.home article a, body.archive article a').hover(function() {
					$(this).children('.work_hover_content').css('visibility','visible').hide().fadeIn(350);
					$(this).children('.work_info').fadeOut(250);
				}, function () {
					$(this).children('.work_hover_content').fadeOut(250);
					$(this).children('.work_info').fadeIn(350);
				});
			});
				
			return false;
		}
		else if (moreBtn == 3) {
			$('#more_posts_wrapper-3').load('more-posts-3/', function() {
				$('body.home article a, body.archive article a').hover(function() {
					$(this).children('.work_hover_content').css('visibility','visible').hide().fadeIn(350);
					$(this).children('.work_info').fadeOut(250);
				}, function () {
					$(this).children('.work_hover_content').fadeOut(250);
					$(this).children('.work_info').fadeIn(350);
				});
			});
				
			return false;
		}
		else if (moreBtn == 4) {
			$('#more_posts_wrapper-4').load('more-posts-4/', function() {
				$('body.home article a, body.archive article a').hover(function() {
					$(this).children('.work_hover_content').css('visibility','visible').hide().fadeIn(350);
					$(this).children('.work_info').fadeOut(250);
				}, function () {
					$(this).children('.work_hover_content').fadeOut(250);
					$(this).children('.work_info').fadeIn(350);
				});
			});
				
			return false;
		}
		else if (moreBtn == 5) {
			$('#more_posts_wrapper-5').load('more-posts-5/', function() {
				$('body.home article a, body.archive article a').hover(function() {
					$(this).children('.work_hover_content').css('visibility','visible').hide().fadeIn(350);
					$(this).children('.work_info').fadeOut(250);
				}, function () {
					$(this).children('.work_hover_content').fadeOut(250);
					$(this).children('.work_info').fadeIn(350);
				});
			});
				
			return false;
		}
		else if (moreBtn == 6) {
			$('#more_posts_wrapper-6').load('more-posts-6/', function() {
				$('body.home article a, body.archive article a').hover(function() {
					$(this).children('.work_hover_content').css('visibility','visible').hide().fadeIn(350);
					$(this).children('.work_info').fadeOut(250);
				}, function () {
					$(this).children('.work_hover_content').fadeOut(250);
					$(this).children('.work_info').fadeIn(350);
				});
			});
				
			return false;
		}
		else if (moreBtn == 7) {
			$('#more_posts_wrapper-7').load('more-posts-7/', function() {
				$('body.home article a, body.archive article a').hover(function() {
					$(this).children('.work_hover_content').css('visibility','visible').hide().fadeIn(350);
					$(this).children('.work_info').fadeOut(250);
				}, function () {
					$(this).children('.work_hover_content').fadeOut(250);
					$(this).children('.work_info').fadeIn(350);
				});
			});
				
			return false;
		}
		else if (moreBtn == 8) {
			$('#more_posts_wrapper-8').load('more-posts-8/', function() {
				$('body.home article a, body.archive article a').hover(function() {
					$(this).children('.work_hover_content').css('visibility','visible').hide().fadeIn(350);
					$(this).children('.work_info').fadeOut(250);
				}, function () {
					$(this).children('.work_hover_content').fadeOut(250);
					$(this).children('.work_info').fadeIn(350);
				});
			});
				
			return false;
		}
	});
	
   
   
  // resize videos
  var $els = $('iframe');
    $('iframe,embed,object').not($els).each(function(){
     $(this).removeAttr('width');
     $(this).removeAttr('height');
    });
  
  
  
  
  
  
  

   
  
  
  // bug orientation portrait/lanscape IOS //
	if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
	var viewportmeta = document.querySelector('meta[name="viewport"]');
	if (viewportmeta) {
		viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
		document.addEventListener('orientationchange', function () {
			viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1';
		}, false);
	  }
	}
   
   
  


// flex slider
	// contact slideshow
	$('body.page-id-7 .flexslider').flexslider({
		animation: "slide",
		controlNav: false,
		directionNav: true,
		slideshow: false,
		itemWidth: 1390
		// randomize: true
	});
	// studio slideshow
    $('body.page-id-6 .flexslider').flexslider({
		animation: "slide",
		controlNav: false,
		directionNav: true,
		slideshow: false,
		itemWidth: 695,
    	minItems: 2,
    	maxItems: 2
	});
	
	
	// create slider hover divs
  	$('.flex-next').append("<div class='slider_right_hover'></div>");
	$('.flex-prev').append("<div class='slider_left_hover'></div>");
	
	
	// studio page vars for color change
  	var studiopage = 1;
  	var numImagesStu = $('body.page-id-6 .flex-viewport ul.slides li').length;
	
	$('body.page-id-6 .flex-next').live('click', function() {
		// scroll right
		if (studiopage < (numImagesStu - 1)) {
			studiopage = (studiopage + 2);
			$('body.page-id-6 .flex-viewport ul.slides li img').delay(200).removeClass('inColor');
			$('body.page-id-6 .flex-viewport ul.slides li:nth-child(' + (studiopage+1) + ') img').delay(500).addClass('inColor');
		} else {
			studiopage = 1;
			$('body.page-id-6 .flex-viewport ul.slides li img').delay(200).removeClass('inColor');
			$('body.page-id-6 .flex-viewport ul.slides li:nth-child(' + (studiopage+1) + ') img').delay(500).addClass('inColor');
		}
	});
	$('body.page-id-6 .flex-prev').live('click', function() {
		// scroll left
		if (studiopage > (2)) {
			studiopage = (studiopage - 2);
			$('body.page-id-6 .flex-viewport ul.slides li img').delay(200).removeClass('inColor');
			$('body.page-id-6 .flex-viewport ul.slides li:nth-child(' + studiopage + ') img').delay(500).addClass('inColor');
		} else {
			studiopage = (numImagesStu - 1);
			$('body.page-id-6 .flex-viewport ul.slides li img').delay(200).removeClass('inColor');
			$('body.page-id-6 .flex-viewport ul.slides li:nth-child(' + studiopage + ') img').delay(500).addClass('inColor');
		}
	});
			
	
	// hover side bars
   $('.flex-next').hover(function() {
	   $('.slider_right_hover').css('opacity', ('1.0'));
	   $('body.page-id-6 .flex-viewport ul.slides li:nth-child(' + (studiopage+1) + ') img').addClass('inColor');
   }, function () {
		$('.slider_right_hover').css('opacity', ('0.0'));
		$('body.page-id-6 .flex-viewport ul.slides li:nth-child(' + (studiopage+1) + ') img').removeClass('inColor');
		
   });
   
   $('.flex-prev').hover(function() {
		$('.slider_left_hover').css('opacity', ('1.0')); 
		$('body.page-id-6 .flex-viewport ul.slides li:nth-child(' + studiopage + ') img').addClass('inColor');
   }, function () {
		$('.slider_left_hover').css('opacity', ('0.0')); 
		$('body.page-id-6 .flex-viewport ul.slides li:nth-child(' + studiopage + ') img').removeClass('inColor');
   });
   
   
   // sidebar heights
   $('.flex-viewport ul.slides li:nth-child(1) img').load(function(){
		 sizeSliderBtns();
	});
	  
	
	
});


function sizeSliderBtns() {
	var sliderHeight = $('.flex-viewport ul.slides li:nth-child(1) img').height();
	$('.flex-next, .flex-prev, .slider_left_hover, .slider_right_hover').css('height', (sliderHeight));
};



$(window).load('resize', sizeSliderBtns);
$(window).bind('resize', sizeSliderBtns);