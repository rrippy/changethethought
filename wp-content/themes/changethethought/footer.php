<div class="push"></div>

</div><!-- #wrapper -->

<footer id="footer" class="clearfix">

	<div id="footer_content">
    
    	 <div class="right_contact_block">
        	<div class="social_links">
            	<a href="http://www.facebook.com/groups/54988984752/" class="facebook"></a>
                <a href="https://twitter.com/changethought" class="twitter"></a>
                <a href="http://www.linkedin.com/pub/christopher-cox/1/545/220" class="linkedin"></a>
            </div>
        	<div class="copyright">&copy;Copyright <?php bloginfo( 'name' ); ?> 2013.</div>
        </div><!-- .right_contact_block -->
        
    
    	<div class="visit_links">
        	Visit: <a href="http://www.changethethought.com/">Our Blog</a> <a href="http://changethethought.bigcartel.com/">Our Store</a>
        </div><!-- .visit_links -->
        
        <div class="contact_details">303 917 3046 | <a href="mailto:hire@changethethought.us">hire@changethethought.us</a></div>
        
	
    		<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'footer-menu' ) ); ?>

    		
        
   </div><!-- #footer_content -->
                    
   
</footer>

</div><!-- #wrapper_all -->


<?php wp_footer(); ?>

  </body>
</html>