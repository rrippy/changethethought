<?php
/**
 * The template for displaying all posts.
 *
 * Default Post Template
 *
 */

get_header(); ?>

	<div id="main" class="clearfix">
    
        
        
   
   <div class="section_main_content">
		
		<div id="primary">
		<?php while ( have_posts() ) : the_post(); ?>
        
        	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="to_projects">
                <div class="back_info">
                
                    <?php $currentID = get_the_ID(); ?>
                        <?php $currentNumber = Get_Post_Number($currentID); ?>
                        <div class="post_number"><?php echo $currentNumber; ?></div>
                        
                    <div class="back_text">Back to Projects</div>
                    
                </div><!--.back_info-->
            </a>
            
  			
            <!-- variable classes to remove prev and next posts for first and last items -->
            <?php 
			$workposts = get_posts(array(
				'post_type' => 'post',
				'numberposts' => 300,
				'post_status' => 'publish',
				'category' => 'animation+build+concept+design+expereintial+film+illustration'
			));
			
            $totalPosts = count($workposts);
            if ($totalPosts == $currentNumber) {
				$currentPost = ('last-post');
			} else {
				$currentPost = ('work_') . $currentNumber;
			}
			?>
            
            
            
             <article id="post-<?php the_ID(); ?>" <?php post_class($currentPost); ?>>
             
             	<?php
					$work_images = get_field('work_images');
					$videos = get_field('videos');
					$letter_code = get_field('letter_code');
					$publish_year = get_field('publish_year');	
				?>
             
             
             	<header>
                
					<?php if ($letter_code) { ?>
                        <div class="letter_code"><?= $letter_code ?></div>
                    <?php } ?>
                    
                    <div class="tagging">
						<?php if ($publish_year) { ?>
                            <div class="publish"><?= $publish_year ?></div>
                        <?php } ?>
                
                		<div class="work_category">Tagged as: <?php the_category(); ?></div> 
                    </div><!--.tagging-->
					
                    <h1><?php the_title();?></h1>

                </header>
             
                <?php if(get_the_content()) { ?>
					<div class="work_description"><?php the_content();?></div>      
        		<?php } ?>
                
                
                
                
                
                <?php if($videos): ?>
                
                	<div class="work_videos">
          
						<?php foreach($videos as $video): ?>

							<div class="video"><?= $video['video'] ?>"</div>
                    
            			<?php endforeach; ?>
                        
                        <?php bootstrapwp_content_nav('nav-below');?>
                        
                    </div><!--.work_main_images-->

       			 <?php endif;?>
                 
                
                
                
                <?php if($work_images): ?>
                
                	<div class="work_main_images">
          
						<?php foreach($work_images as $work_image): ?>
							
                            <?php if($work_image['work_image']): ?>
                    			<img src="<?= $work_image['work_image'] ?>" />
                			<?php endif; ?>
                    
            			<?php endforeach; ?>
                        
                        <?php if($videos) {
						} else { ?>
                        	<?php bootstrapwp_content_nav('nav-below');?>
                        <?php } ?>
                        
                    </div><!--.work_main_images-->

       			 <?php endif;?>
                 
              
           </article>
        
        <?php endwhile; // End the loop ?>
        
        
    	<div class="top_button"><a href="#">Top</a></div>
        
        </div><!-- #primary -->

        
        
    </div><!-- .section_main_content -->
    
    
    </div><!-- #main -->



<?php get_footer(); ?>