<?php
    /*
        Template Name: Additional Posts - 7
    */
	
    $offset = htmlspecialchars(trim($_GET['offset']));
    if ($offset == '') {
        $offset = 63;
    }
   
    $query = new WP_Query('posts_per_page=9&offset=' . $offset); 
?>


    <?php  
	
	 
        while ($query->have_posts()) : $query->the_post(); ?>
        
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
             <a href="<?php the_permalink(); ?>" class="work_thumb">
             
             	<?php the_post_thumbnail(); ?>
                
                <div class="work_hover_content">
                
                	<header>
                        <h2><?php the_title();?></h2>
                        <div class="work_tagged"><strong>Tags: </strong> 
							<?php $category = get_the_category(); 
								echo $category[0]->cat_name; ?>
                        </div>
                        <div class="view_project">View Project</div>
                    </header>
                  
                </div><!--.work_hover_content-->
                
                
                
                <div class="work_info">
                
                	<?php $letter_code = get_field('letter_code'); ?>
                    <div class="letter_code"><?= $letter_code ?></div>
                    
					<?php $currentID = get_the_ID(); ?>
                    <?php $currentNumber = Get_Post_Number($currentID); ?>
                    <div class="post_number"><?php echo $currentNumber; ?></div>  
                
                 </div><!--.work_info-->

                
           </a> <!--.work_thumb-->  
           </article>
           
    <?php  endwhile; ?>
		
	<div id="more_posts_wrapper-8"></div>
    